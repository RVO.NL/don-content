{
  "$schema": "http://json-schema.org/draft-07/schema",
  "$id": "https://gitlab.com/commonground/don/don-content/-/raw/main/schema/don-api.json",
  "title": "API definition",
  "description": "Describes an API on developer.overheid.nl",
  "type": "object",
  "properties": {
    "service_name": {
      "description": "The name of the API",
      "type": "string",
      "minLength": 1
    },
    "description": {
      "description": "The description of the API",
      "type": "string",
      "minLength": 1
    },
    "organization": {
      "$ref": "#/definitions/organization"
    },
    "api_type": {
      "$ref": "#/definitions/apiType"
    },
    "api_authentication": {
      "$ref": "#/definitions/apiAuthentication"
    },
    "tags": {
      "type": "array",
      "description": "A list of tags",
      "items": {
        "type": "string",
        "minLength": 1
      },
      "uniqueItems": true,
      "minItems": 1
    },
    "environments": {
      "$ref": "#/definitions/environments"
    },
    "contact": {
      "$ref": "#/definitions/contact"
    },
    "is_reference_implementation": {
      "type": "boolean",
      "description": "If the API is a reference implementation of a standard"
    },
    "terms_of_use": {
      "$ref": "#/definitions/termsOfUse"
    },
    "forum": {
      "$ref": "#/definitions/forum"
    }
  },
  "required": [
    "service_name",
    "description",
    "organization",
    "api_type",
    "api_authentication",
    "environments"
  ],
  "additionalProperties": false,
  "definitions": {
    "organization": {
      "description": "The owning organization of the repositories",
      "type": "object",
      "properties": {
        "name": {
          "description": "Name of the organization",
          "type": "string",
          "minLength": 1
        },
        "ooid": {
          "description": "The ID of the organization as seen on organisaties.overheid.nl",
          "type": "integer",
          "minimum": 0
        }
      },
      "required": ["name", "ooid"],
      "additionalProperties": false
    },
    "apiType": {
      "description": "The type of API",
      "oneOf": [
        {
          "enum": ["odata"],
          "description": "An OData (Open Data Protocol) API"
        },
        {
          "enum": ["rest_json"],
          "description": "A REST JSON API"
        },
        {
          "enum": ["rest_xml"],
          "description": "A REST XML API"
        },
        {
          "enum": ["soap_xml"],
          "description": "A SOAP API"
        },
        {
          "enum": ["grpc"],
          "description": "A gRCP API"
        },
        {
          "enum": ["graphql"],
          "description": "A GraphQL API"
        },
        {
          "enum": ["sparql"],
          "description": "A SPARQL API"
        },
        {
          "enum": ["wfs"],
          "description": "A Web Feature Service (WFS) API"
        },
        {
          "enum": ["wms"],
          "description": "A Web Map Service (WMS) API"
        },
        {
          "enum": ["unknown"],
          "description": "The API type is unknown"
        }
      ]
    },
    "apiAuthentication": {
      "description": "What authentication method to use",
      "oneOf": [
        {
          "enum": ["none"],
          "description": "No authentication is required"
        },
        {
          "enum": ["mutual_tls"],
          "description": "Mutial TLS is required"
        },
        {
          "enum": ["api_key"],
          "description": "An API key is required"
        },
        {
          "enum": ["ip_allow_list"],
          "description": "The IP of the client needs to be allowed explicitly"
        },
        {
          "enum": ["oauth2"],
          "description": "Use OAuth 2.0 to authorize"
        },
        {
          "enum": ["unknown"],
          "description": "It is unknown how to authenticate"
        }
      ]
    },
    "environments": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "description": "The name of the environment",
            "enum": ["production", "acceptance", "demo"]
          },
          "api_url": {
            "type": "string",
            "description": "The base URL to the API",
            "format": "uri",
            "pattern": "^https"
          },
          "specification_url": {
            "type": "string",
            "description": "The URL to the OpenAPI Specification (OAS) document",
            "format": "uri",
            "pattern": "^https"
          },
          "documentation_url": {
            "type": "string",
            "description": "The URL to the (developer) documentation",
            "format": "uri",
            "pattern": "^https"
          }
        },
        "required": ["name", "api_url"],
        "additionalProperties": false
      },
      "uniqueItems": true,
      "minItems": 1
    },
    "contact": {
      "description": "How to make contact with the owner of the API",
      "properties": {
        "email":{
          "type": "string",
          "format": "email"
        },
        "phone": {
          "type": "string",
          "minLength": 1
        },
        "url": {
          "type": "string",
          "format": "uri",
          "pattern": "^https"
        }
      },
      "minProperties": 1,
      "additionalProperties": false
    },
    "termsOfUse": {
      "description": "The terms on how to use the API",
      "properties": {
        "government_only": {
          "type": "boolean",
          "description": "If the API only can be used by the government"
        },
        "pay_per_use": {
          "type": "boolean",
          "description": "If the usage of the API needs to be payed for"
        },
        "uptime_guarantee": {
          "type": "number",
          "description": "The uptime guarantee in percentages"
        },
        "support_response_time": {
          "type": "number",
          "description": "The support response time, in working days",
          "minimum": 0
        }
      },
      "minProperties": 1,
      "additionalProperties": false
    },
    "forum": {
      "description": "The forum where can be discussed about the API",
      "properties": {
        "vendor": {
          "description": "The vendor of the forum software",
          "enum": ["discourse"]
        },
        "url": {
          "type": "string",
          "description": "The URL to the discussion forum of the API",
          "format": "uri",
          "pattern": "^https"
        }
      },
      "required": ["vendor", "url"],
      "additionalProperties": false
    }
  }
}
